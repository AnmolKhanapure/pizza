<?php

?>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login Form</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/main.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/main.js"></script>
		<style type="text/css">
		a:link {color: #ffffff}
		a:visited {color: #ffffff}
		a:hover {color: #ffffff}
		a:active {color: #ffffff}
        font
        {
            color: white;
            background-color: black;
            font-family: roboto;
	    }
        </style>
    </head>
    <body background="img/bg11.jpg">
        <FONT size="5" color="white">
            <?php include("header.php") ?>
            <form id="login-form" class="login-form" name="form1" method="post" action="register.php">
                <input type="hidden" name="is_login" value="1">
                <div class="h1">Register</h1><br><br>
                <div id="form-content">
                    <div class="form-group">
                        <label for="fname">First Name</label>
                        <div><input name="fname" type="TEXT" placeholder="Enter your first name" size="30" maxlength="30" align="center" id="fname"/></div>
                    </div>
                    <div class="form-group">
                        <label for="lname">Last Name</label>
                        <div><input name="lname" type="text" placeholder="Enter your last name" size="30" maxlength="30" align="center" id="lname"/></div>
                    </div>
                    <div class="form-group">
                        <label for="location">
                         <div><SELECT name="location" id="location" style="color:#333;" onchange="document.postElementById('location').style.color='black';">
                                                <OPTION VALUE="none" disabled selected>-------SELECT YOUR LOCATION-------</OPTION>
                                                <OPTION VALUE="Katraj" style="color:black;">Katraj</OPTION>
                                                <OPTION VALUE="Kothrud" style="color:black;">Kothrud</OPTION>
                                                <OPTION VALUE="Baner East" style="color:black;">Baner</OPTION>
                                                <OPTION VALUE="Bibewadi" style="color:black;">Bibewadi</OPTION>
                                               </SELECT>
                        </div>
    </div>
    </body>
</html>